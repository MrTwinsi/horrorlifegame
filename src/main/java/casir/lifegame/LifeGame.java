package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class LifeGame {


    public LifeGame() {
        Board oldPlayBoard = new Board(20);
        Board playBoard = new Board(20);
        playBoard.content[17][17] = true; //remplissage des cases par défaut pour le début du jeu
        playBoard.content[18][17] = true;
        playBoard.content[17][18] = true;
        playBoard.content[19][18] = true;
        playBoard.content[17][19] = true;

        while (!playBoard.equals(oldPlayBoard)) {
            UserInterface.displayGameBoard(playBoard);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            oldPlayBoard = playBoard.clone();
            playBoard = new Board(20);
            for (int x = 0; x < 20; x++) {
                for (int y = 0; y < 20; y++) {
                    int nb = 0;
                    for (int v = -1; v <= 1; v++) {
                        for (int h = -1; h <= 1; h++) {
                            if (!(v == 0 && h == 0)) {
                                if (x + h >= 0 && x + h < 20 && y + v >= 0 && y + v < 20) {
                                    boolean ok = oldPlayBoard.content[x + h][y + v];
                                    if (ok) {
                                        nb++;
                                    }
                                }
                            }
                        }
                    }
                    if (oldPlayBoard.content[x][y]) {
                        if (nb < 2 || nb > 3) {
                            playBoard.content[x][y] = false;
                        } else {
                            playBoard.content[x][y] = true;
                        }
                    } else {
                        if (nb == 3) {
                            playBoard.content[x][y] = true;
                        } else {
                            playBoard.content[x][y] = false;
                        }
                    }
                }
            }
        }
        UserInterface.displayEndMessage();
    }
}