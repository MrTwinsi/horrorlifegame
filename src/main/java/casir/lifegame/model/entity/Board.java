package casir.lifegame.model.entity;

public class Board {

    public boolean[][] content;

    public Board(int j){
        this.content = new boolean[j][j];
        boolean[] l;
        for (int x=0; x<j; x++){
            l = new boolean[j];
            for (int y=0; y<j; y++) {
                l[y] = false;
            }
            this.content[x] = l;
        }
    }

    public boolean equals(Board board2) {
        for (int x=0; x<this.content.length; x++){
            for (int y=0; y<this.content.length; y++) {
                if (this.content[x][y] != board2.content[x][y]){
                    return false;
                }
            }
        }
        return true;
    }

    public Board clone(){
        Board clone = new Board(this.content.length);
        for (int x=0; x<this.content.length; x++){
            for (int y=0; y<this.content.length; y++) {
                clone.content[x][y] = this.content[x][y];
            }
        }
        return clone;
    }
}